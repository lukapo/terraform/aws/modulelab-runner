# GITLAB-RUNNER MODULE

Provider Requirements:
* **aws:** (any version)
* **null:** (any version)
* **template:** (any version)

## Input Variables
* `ami_filter` (default `{"name":["amzn-ami-hvm-2018.03*-x86_64-ebs"]}`): List of maps used to create the AMI filter for the Gitlab runner agent AMI. Currently Amazon Linux 2 `amzn2-ami-hvm-2.0.????????-x86_64-ebs` looks to *not* be working for this configuration.
* `ami_owners` (default `["amazon"]`): The list of owners used to select the AMI of Gitlab runner agent instances.
* `aws_region` (default `"eu-central-1"`): AWS region.
* `aws_zone` (default `"a"`): AWS availability zone (typically 'a', 'b', or 'c').
* `cache_bucket` (default `{"bucket":"","create":true,"policy":""}`): Configuration to control the creation of the cache bucket. By default the bucket will be created and used as shared cache. To use the same cache cross multiple runners disable the cration of the cache and provice a policy and bucket name. See the public runner example for more details.
* `cache_bucket_name_include_account_id` (default `true`): Boolean to add current account ID to cache bucket name.
* `cache_bucket_prefix` (required): Prefix for s3 cache bucket name.
* `cache_bucket_versioning` (required): Boolean used to enable versioning on the cache bucket, false by default.
* `cache_expiration_days` (default `1`): Number of days before cache objects expires.
* `cache_shared` (required): Enables cache sharing between runners, false by default.
* `docker_machine_instance_type` (default `"m5a.large"`): Instance type used for the instances hosting docker-machine.
* `docker_machine_options` (required): List of additional options for the docker machine config. Each element of this list must be a key=value pair. E.g. '["amazonec2-zone=a"]'
* `docker_machine_role_json` (required): Docker machine runner instance override policy, expected to be in JSON format.
* `docker_machine_spot_price_bid` (default `"0.06"`): Spot price bid.
* `docker_machine_version` (default `"0.16.2"`): Version of docker-machine.
* `enable_manage_gitlab_token` (default `true`): Boolean to enable the management of the GitLab token in SSM. If `true` the token will be stored in SSM, which means the SSM property is a terraform managed resource. If `false` the Gitlab token will be stored in the SSM by the user-data script during creation of the the instance. However the SSM parameter is not managed by terraform and will remain in SSM after a `terraform destroy`.
* `environment` (required): A name that identifies the environment, used as prefix and for tagging.
* `gitlab_runner_registration_config` (default `{"access_level":"","description":"","locked_to_project":"","maximum_timeout":"","registration_token":"","run_untagged":"","tag_list":""}`): Configuration used to register the runner.
* `gitlab_runner_version` (default `"12.5.0"`): Version of the GitLab runner.
* `instance_role_json` (required): Default runner instance override policy, expected to be in JSON format.
* `instance_type` (default `"t3.micro"`): Instance type used for the GitLab runner.
* `name` (required): Base name to use for all the resources
* `overrides` (default `{"name_docker_machine_runners":"","name_runner_agent_instance":"","name_sg":""}`): This maps provides the possibility to override some defaults. The following attributes are supported: `name_sg` overwrite the `Name` tag for all security groups created by this module. `name_runner_agent_instance` override the `Name` tag for the ec2 instance defined in the auto launch configuration. `name_docker_machine_runners` ovverrid the `Name` tag spot instances created by the runner agent.
* `runner_ami_filter` (default `{"name":["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]}`): List of maps used to create the AMI filter for the Gitlab runner docker-machine AMI.
* `runner_ami_owners` (default `["099720109477"]`): The list of owners used to select the AMI of Gitlab runner docker-machine instances.
* `runner_instance_spot_price` (required): By setting a spot price bid price the runner agent will be created via a spot request. Be aware that spot instances can be stopped by AWS.
* `runner_root_block_device` (required): The EC2 instance root block device configuration. Takes the following keys: `delete_on_termination`, `volume_type`, `volume_size`, `iops`
* `runners_additional_volumes` (required): Additional volumes that will be used in the runner config.toml, e.g Docker socket
* `runners_concurrent` (default `10`): Concurrent value for the runners, will be used in the runner config.toml.
* `runners_environment_vars` (required): Environment variables during build execution, e.g. KEY=Value, see runner-public example. Will be used in the runner config.toml
* `runners_executor` (default `"docker+machine"`): The executor to use. Currently supports `docker+machine` or `docker`.
* `runners_gitlab_url` (required): URL of the GitLab instance to connect to.
* `runners_iam_instance_profile_name` (required): IAM instance profile name of the runners, will be used in the runner config.toml
* `runners_idle_count` (required): Idle count of the runners, will be used in the runner config.toml.
* `runners_idle_time` (default `600`): Idle time of the runners, will be used in the runner config.toml.
* `runners_image` (default `"docker:18.03.1-ce"`): Image to run builds, will be used in the runner config.toml
* `runners_limit` (required): Limit for the runners, will be used in the runner config.toml.
* `runners_max_builds` (required): Max builds for each runner after which it will be removed, will be used in the runner config.toml. By default set to 0, no maxBuilds will be set in the configuration.
* `runners_name` (required): Name of the runner, will be used in the runner config.toml.
* `runners_off_peak_idle_count` (required): Off peak idle count of the runners, will be used in the runner config.toml.
* `runners_off_peak_idle_time` (required): Off peak idle time of the runners, will be used in the runner config.toml.
* `runners_off_peak_periods` (required): Off peak periods of the runners, will be used in the runner config.toml.
* `runners_off_peak_timezone` (required): Off peak idle time zone of the runners, will be used in the runner config.toml.
* `runners_output_limit` (default `4096`): Sets the maximum build log size in kilobytes, by default set to 4096 (4MB)
* `runners_post_build_script` (required): Commands to be executed on the Runner just after executing the build, but before executing after_script. 
* `runners_pre_build_script` (required): Script to execute in the pipeline just before the build, will be used in the runner config.toml
* `runners_pre_clone_script` (required): Commands to be executed on the Runner before cloning the Git repository. this can be used to adjust the Git client configuration first, for example. 
* `runners_privileged` (default `true`): Runners will run in privileged mode, will be used in the runner config.toml
* `runners_pull_policy` (default `"always"`): pull_policy for the runners, will be used in the runner config.toml
* `runners_request_concurrency` (default `1`): Limit number of concurrent requests for new jobs from GitLab (default 1)
* `runners_request_spot_instance` (default `true`): Whether or not to request spot instances via docker-machine
* `runners_root_size` (default `16`): Runner instance root size in GB.
* `runners_services_volumes_tmpfs` (required): Mount temporary file systems to service containers. Must consist of pairs of strings e.g. "/var/lib/mysql" = "rw,noexec", see example
* `runners_shm_size` (required): shm_size for the runners, will be used in the runner config.toml
* `runners_token` (default `"__REPLACED_BY_USER_DATA__"`): Token for the runner, will be used in the runner config.toml.
* `runners_use_private_address` (default `true`): Restrict runners to the use of a private IP address
* `runners_volumes_tmpfs` (required): Mount temporary file systems to the main containers. Must consist of pairs of strings e.g. "/var/lib/mysql" = "rw,noexec", see example
* `secure_parameter_store_runner_token_key` (default `"runner-token"`): The key name used store the Gitlab runner token in Secure Parameter Store
* `subnet_id_runners` (required): List of subnets used for hosting the gitlab-runners.
* `subnet_ids_gitlab_runner` (required): Subnet used for hosting the GitLab runner.
* `tags` (required): Map of tags that will be added to created resources. By default resources will be tagged with name and environment.
* `timezone` (default `"Europe/Amsterdam"`): Name of the timezone that the runner will be used in.
* `vpc_id` (required): The target VPC for the docker-machine and runner instances.

## Output Values
* `runner_agent_role_arn`: ARN of the role used for the ec2 instance for the GitLab runner agent.
* `runner_agent_role_name`: Name of the role used for the ec2 instance for the GitLab runner agent.
* `runner_agent_sg_id`: ID of the security group attached to the GitLab runner agent.
* `runner_as_group_name`: Name of the autoscaling group for the gitlab-runner instance
* `runner_cache_bucket_arn`: ARN of the S3 for the build cache.
* `runner_cache_bucket_name`: Name of the S3 for the build cache.
* `runner_role_arn`: ARN of the role used for the docker machine runners.
* `runner_role_name`: Name of the role used for the docker machine runners.
* `runner_sg_id`: ID of the security group attached to the docker machine runners.

## Managed Resources
* `aws_autoscaling_group.gitlab_runner_instance` from `aws`
* `aws_iam_instance_profile.docker_machine` from `aws`
* `aws_iam_instance_profile.instance` from `aws`
* `aws_iam_policy.instance_docker_machine_policy` from `aws`
* `aws_iam_policy.ssm` from `aws`
* `aws_iam_role.docker_machine` from `aws`
* `aws_iam_role.instance` from `aws`
* `aws_iam_role_policy_attachment.docker_machine_cache_instance` from `aws`
* `aws_iam_role_policy_attachment.instance_docker_machine_policy` from `aws`
* `aws_iam_role_policy_attachment.ssm` from `aws`
* `aws_launch_configuration.gitlab_runner_instance` from `aws`
* `aws_security_group.docker_machine` from `aws`
* `aws_security_group.runner` from `aws`
* `aws_security_group_rule.docker_machine_docker_runner` from `aws`
* `aws_security_group_rule.docker_machine_docker_self` from `aws`
* `aws_security_group_rule.out_all` from `aws`
* `aws_ssm_parameter.runner_registration_token` from `aws`

## Data Resources
* `data.aws_ami.docker-machine` from `aws`
* `data.aws_ami.runner` from `aws`
* `data.aws_caller_identity.current` from `aws`
* `data.null_data_source.tags` from `null`
* `data.template_file.dockermachine_role_trust_policy` from `template`
* `data.template_file.gitlab_runner` from `template`
* `data.template_file.instance_docker_machine_policy` from `template`
* `data.template_file.instance_role_trust_policy` from `template`
* `data.template_file.runners` from `template`
* `data.template_file.services_volumes_tmpfs` from `template`
* `data.template_file.ssm_policy` from `template`
* `data.template_file.volumes_tmpfs` from `template`

## Child Modules
* `cache` from `./cache`
