provider "aws" {
  region = "eu-central-1"
}

data "aws_availability_zones" "available" {
  state = "available"
}

locals {
  project            = "test"
  environment        = "dev"
  env_project        = "${local.environment}-${local.project}"
  aws_region         = "eu-central-1"
  gitlab_url         = "https://gitlab.com"
  registration_token = "XsX"
  timezone           = "Europe/Amsterdam"
}

module "vpc" {
  source = "git::https://gitlab.com/lukapo/terraform/aws/module.vpc.git?ref=1.0.1"

  vpc_name   = "vpc-${local.environment}"
  aws_region = local.aws_region
  cidr       = "10.0.0.0/16"

  azs             = [data.aws_availability_zones.available.names[0]]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_s3_endpoint = true

  tags = {
    Environment = local.environment
  }
}

module "runner" {
  source = "../"

  aws_region  = local.aws_region
  environment = local.environment
  name        = local.project

  vpc_id                   = module.vpc.vpc_id
  subnet_ids_gitlab_runner = module.vpc.private_subnets
  subnet_id_runners        = element(module.vpc.private_subnets, 0)

  runners_name       = "${local.env_project}-gitlab-runner"
  runners_gitlab_url = local.gitlab_url

  docker_machine_instance_type = "t2.small"

  docker_machine_spot_price_bid = "0.06"

  gitlab_runner_registration_config = {
    registration_token = local.registration_token
    tag_list           = "docker_spot_runner"
    description        = "runner default - auto"
    locked_to_project  = "true"
    run_untagged       = "false"
    maximum_timeout    = "3600"
  }

  tags = {
    "tf-aws-gitlab-runner:example"           = "runner-default"
    "tf-aws-gitlab-runner:instancelifecycle" = "spot:yes"
  }

  runners_off_peak_timezone   = local.timezone
  runners_off_peak_idle_count = 0
  runners_off_peak_idle_time  = 60

  runners_privileged         = "true"
  runners_additional_volumes = ["/certs/client"]
  runners_extra_hosts        = ["test.gitlab:1.2.3.4"]

  runners_volumes_tmpfs = [
    { "/var/opt/cache" = "rw,noexec" },
  ]

  runners_services_volumes_tmpfs = [
    { "/var/lib/mysql" = "rw,noexec" },
  ]
  # working 9 to 5 :)
  runners_off_peak_periods = "[\"* * 0-9,17-23 * * mon-fri *\", \"* * * * * sat,sun *\"]"
}
