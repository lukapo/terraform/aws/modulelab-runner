locals {
  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    {
      "Environment" = format("%s", var.environment)
    },
    var.tags,
  )

  tags_string = join(",", flatten([
    for key in keys(local.tags) : [key, lookup(local.tags, key)]
  ]))

  runners_additional_volumes = <<-EOT
  %{~for volume in var.runners_additional_volumes~},"${volume}"%{endfor~}
  EOT

  runners_extra_hosts = <<-EOT
  %{~for host in var.runners_extra_hosts~}"${host}",%{endfor~}
  EOT
}

data "null_data_source" "tags" {
  count = length(local.tags)

  inputs = {
    key                 = element(keys(local.tags), count.index)
    value               = element(values(local.tags), count.index)
    propagate_at_launch = "true"
  }
}

data "aws_caller_identity" "current" {}

resource "aws_security_group" "runner" {
  name_prefix = "security_group-${var.name}-gitlab-runner"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "security_group-${var.name}-gitlab-runner"
  }
}

resource "aws_security_group_rule" "ssh_ingress" {
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.runner.id
}

resource "aws_security_group" "docker_machine" {
  name_prefix = "security_group-${var.name}-docker-machine"
  vpc_id      = var.vpc_id

  tags = {
    Name = "sg-${var.name}-docker-machine"
  }
}

resource "aws_security_group_rule" "docker_machine_docker_runner" {
  type                     = "ingress"
  from_port                = 2376
  to_port                  = 2376
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.runner.id

  security_group_id = aws_security_group.docker_machine.id
}

resource "aws_security_group_rule" "docker_machine_docker_self" {
  type      = "ingress"
  from_port = 2376
  to_port   = 2376
  protocol  = "tcp"
  self      = true

  security_group_id = aws_security_group.docker_machine.id
}

resource "aws_security_group_rule" "out_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.docker_machine.id
}

# Parameter value is managed by the user-data script of the gitlab runner instance
resource "aws_ssm_parameter" "runner_registration_token" {
  name  = "${var.environment}-${var.secure_parameter_store_runner_token_key}"
  type  = "SecureString"
  value = "null"

  tags = {
    Name        = var.name,
    Environment = var.environment
  }

  lifecycle {
    ignore_changes = [value]
  }
}

data "template_file" "gitlab_runner" {
  template = file("${path.module}/scripts/gitlab-runner-user-data.tpl")

  vars = {
    gitlab_runner_version                   = var.gitlab_runner_version
    docker_machine_version                  = var.docker_machine_version
    runners_config                          = data.template_file.runners.rendered
    runners_executor                        = var.runners_executor
    runners_gitlab_url                      = var.runners_gitlab_url
    runners_token                           = var.runners_token
    secure_parameter_store_runner_token_key = var.secure_parameter_store_runner_token_key
    secure_parameter_store_region           = var.aws_region
    gitlab_runner_registration_token        = var.gitlab_runner_registration_config["registration_token"]
    giltab_runner_description               = var.gitlab_runner_registration_config["description"]
    gitlab_runner_tag_list                  = var.gitlab_runner_registration_config["tag_list"]
    gitlab_runner_locked_to_project         = var.gitlab_runner_registration_config["locked_to_project"]
    gitlab_runner_run_untagged              = var.gitlab_runner_registration_config["run_untagged"]
    gitlab_runner_maximum_timeout           = var.gitlab_runner_registration_config["maximum_timeout"]
    gitlab_runner_access_level              = lookup(var.gitlab_runner_registration_config, "access_level", "not_protected")
    gitlab_runner_extra_hosts               = local.runners_extra_hosts
  }
}

data "template_file" "services_volumes_tmpfs" {
  template = file("${path.module}/templates/volumes.tpl")
  count    = length(var.runners_services_volumes_tmpfs)
  vars = {
    volume  = element(keys(var.runners_services_volumes_tmpfs[count.index]), 0)
    options = element(values(var.runners_services_volumes_tmpfs[count.index]), 0)
  }
}

data "template_file" "volumes_tmpfs" {
  template = file("${path.module}/templates/volumes.tpl")
  count    = length(var.runners_volumes_tmpfs)
  vars = {
    volume  = element(keys(var.runners_volumes_tmpfs[count.index]), 0)
    options = element(values(var.runners_volumes_tmpfs[count.index]), 0)
  }
}

data "template_file" "runners" {
  template = file("${path.module}/templates/runner-config.tpl")

  vars = {
    aws_region                  = var.aws_region
    gitlab_url                  = var.runners_gitlab_url
    runners_vpc_id              = var.vpc_id
    runners_subnet_id           = var.subnet_id_runners
    runners_aws_zone            = var.aws_zone
    runners_instance_type       = var.docker_machine_instance_type
    runners_spot_price_bid      = var.docker_machine_spot_price_bid
    runners_ami                 = data.aws_ami.docker-machine.id
    runners_security_group_name = aws_security_group.docker_machine.name
    runners_instance_profile    = aws_iam_instance_profile.docker_machine.name
    runners_additional_volumes  = local.runners_additional_volumes
    runners_extra_hosts         = local.runners_extra_hosts
    docker_machine_options      = length(var.docker_machine_options) == 0 ? "" : format(",%s", join(",", formatlist("%q", var.docker_machine_options)), )
    runners_name                = var.runners_name
    runners_tags = var.overrides["name_docker_machine_runners"] == "" ? format(
      "%s,Name,%s-docker-machine",
      local.tags_string,
      var.environment,
      ) : format(
      "%s,Name,%s",
      local.tags_string,
      var.overrides["name_docker_machine_runners"],
    )
    runners_token                     = var.runners_token
    runners_executor                  = var.runners_executor
    runners_limit                     = var.runners_limit
    runners_concurrent                = var.runners_concurrent
    runners_image                     = var.runners_image
    runners_privileged                = var.runners_privileged
    runners_shm_size                  = var.runners_shm_size
    runners_pull_policy               = var.runners_pull_policy
    runners_idle_count                = var.runners_idle_count
    runners_idle_time                 = var.runners_idle_time
    runners_max_builds                = var.runners_max_builds == 0 ? "" : format("MaxBuilds = %d", var.runners_max_builds)
    runners_off_peak_timezone         = var.runners_off_peak_timezone
    runners_off_peak_idle_count       = var.runners_off_peak_idle_count
    runners_off_peak_idle_time        = var.runners_off_peak_idle_time
    runners_off_peak_periods_string   = var.runners_off_peak_periods == "" ? "" : format("OffPeakPeriods = %s", var.runners_off_peak_periods)
    runners_root_size                 = var.runners_root_size
    runners_iam_instance_profile_name = var.runners_iam_instance_profile_name
    runners_use_private_address_only  = var.runners_use_private_address
    runners_use_private_address       = ! var.runners_use_private_address
    runners_request_spot_instance     = var.runners_request_spot_instance
    runners_environment_vars          = jsonencode(var.runners_environment_vars)
    runners_pre_build_script          = var.runners_pre_build_script
    runners_post_build_script         = var.runners_post_build_script
    runners_pre_clone_script          = var.runners_pre_clone_script
    runners_request_concurrency       = var.runners_request_concurrency
    runners_output_limit              = var.runners_output_limit
    runners_volumes_tmpfs             = chomp(join("", data.template_file.volumes_tmpfs.*.rendered))
    runners_services_volumes_tmpfs    = chomp(join("", data.template_file.services_volumes_tmpfs.*.rendered))
    bucket_name                       = var.cache_bucket["create"] ? module.cache.bucket : var.cache_bucket["bucket"]
    shared_cache                      = var.cache_shared
  }
}

data "aws_ami" "docker-machine" {
  most_recent = "true"

  dynamic "filter" {
    for_each = var.runner_ami_filter
    content {
      name   = filter.key
      values = filter.value
    }
  }

  owners = var.runner_ami_owners
}

resource "aws_autoscaling_group" "gitlab_runner_instance" {
  name                = "asg-${var.name}"
  vpc_zone_identifier = var.subnet_ids_gitlab_runner

  min_size                  = "1"
  max_size                  = "1"
  desired_capacity          = "1"
  health_check_grace_period = 0
  launch_configuration      = aws_launch_configuration.gitlab_runner_instance.name

  tags = concat(
    data.null_data_source.tags.*.outputs,
    [
      {
        "key"                 = "Name"
        "value"               = var.overrides["name_runner_agent_instance"] == "" ? local.tags["Name"] : var.overrides["name_runner_agent_instance"]
        "propagate_at_launch" = true
      },
    ],
  )

}

data "aws_ami" "runner" {
  most_recent = "true"

  dynamic "filter" {
    for_each = var.ami_filter
    content {
      name   = filter.key
      values = filter.value
    }
  }

  owners = var.ami_owners
}

resource "aws_launch_configuration" "gitlab_runner_instance" {
  security_groups      = [aws_security_group.runner.id]
  image_id             = data.aws_ami.runner.id
  user_data            = data.template_file.gitlab_runner.rendered
  instance_type        = var.instance_type
  spot_price           = var.runner_instance_spot_price
  iam_instance_profile = aws_iam_instance_profile.instance.name
  dynamic "root_block_device" {
    for_each = [var.runner_root_block_device]
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", true)
      volume_type           = lookup(root_block_device.value, "volume_type", "gp2")
      volume_size           = lookup(root_block_device.value, "volume_size", 8)
      iops                  = lookup(root_block_device.value, "iops", null)
    }
  }

  associate_public_ip_address = false == var.runners_use_private_address

  lifecycle {
    create_before_destroy = true
  }
}

module "cache" {
  source = "./cache"

  environment = var.environment
  tags        = local.tags

  create_cache_bucket                  = var.cache_bucket["create"]
  cache_bucket_prefix                  = var.cache_bucket_prefix
  cache_bucket_name_include_account_id = var.cache_bucket_name_include_account_id
  cache_bucket_versioning              = var.cache_bucket_versioning
  cache_expiration_days                = var.cache_expiration_days
  name-runner                          = var.name
}

resource "aws_iam_instance_profile" "instance" {
  name = "${var.environment}-${var.name}-instance-profile"
  role = aws_iam_role.instance.name
}

data "template_file" "instance_role_trust_policy" {
  template = length(var.instance_role_json) > 0 ? var.instance_role_json : file("${path.module}/policies/instance-role-trust-policy.json")
}

resource "aws_iam_role" "instance" {
  name               = "${var.environment}-${var.name}-instance-role"
  assume_role_policy = data.template_file.instance_role_trust_policy.rendered
}

data "template_file" "instance_docker_machine_policy" {
  template = file(
    "${path.module}/policies/instance-docker-machine-policy.json",
  )
}

resource "aws_iam_policy" "instance_docker_machine_policy" {
  name        = "${var.environment}-${var.name}-docker-machine"
  path        = "/"
  description = "Policy for docker machine."

  policy = data.template_file.instance_docker_machine_policy.rendered
}

resource "aws_iam_role_policy_attachment" "instance_docker_machine_policy" {
  role       = aws_iam_role.instance.name
  policy_arn = aws_iam_policy.instance_docker_machine_policy.arn
}

resource "aws_iam_role_policy_attachment" "docker_machine_cache_instance" {
  role       = aws_iam_role.instance.name
  policy_arn = var.cache_bucket["create"] ? module.cache.policy_arn : var.cache_bucket["policy"]
}

data "template_file" "dockermachine_role_trust_policy" {
  template = length(var.docker_machine_role_json) > 0 ? var.docker_machine_role_json : file("${path.module}/policies/instance-role-trust-policy.json")
}

resource "aws_iam_role" "docker_machine" {
  name               = "${var.environment}-${var.name}-docker-machine-role"
  assume_role_policy = data.template_file.dockermachine_role_trust_policy.rendered
}

resource "aws_iam_instance_profile" "docker_machine" {
  name = "${var.environment}-${var.name}-docker-machine-profile"
  role = aws_iam_role.docker_machine.name
}
